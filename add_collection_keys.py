#!/usr/bin/env python3

import json
import sys
from collections import OrderedDict


def main():
    if len(sys.argv) != 2:
        print("Usage: add_collection_keys.py [citations.json]")
        sys.exit(1)
    fname = sys.argv[1]

    with open(fname, 'r') as infile:
        citations = json.load(infile, object_pairs_hook=OrderedDict)

    colls_for_item = {}

    for coll in citations['collections'].values():
        for item in coll['items']:
            if item not in colls_for_item:
                colls_for_item[item] = []
            colls_for_item[item].append(coll['key'])

    # scan to find creator types
    creator_types = {creator['creatorType'] for item in citations['items'] for creator in item['creators']}

    citations['domains'] = {
        'creatorType' : sorted(creator_types)
    }

    for item in citations['items']:
        # assign collection key
        colls = colls_for_item.get(item['itemID'], [])

        if not colls:
            print('No collections for', item['citekey'])

        item['collectionKeys'] = colls

        # reformat authors and editors
        for typ in creator_types:
            item[typ + 's'] = []

        for creator in item['creators']:
            if 'firstName' in creator and 'lastName' in creator:
                entry = {
                    'nameFwd' : '{firstName} {lastName}'.format_map(creator),
                    'nameRev' : '{lastName}, {firstName}'.format_map(creator)
                }
            else:
                entry = {
                    'nameFwd' : creator['name'],
                    'nameRev' : ''
                }

            item[creator['creatorType'] + 's'].append(entry)


    with open(fname, 'w') as outfile:
        json.dump(citations, outfile, indent=2, ensure_ascii=False)


if __name__ == "__main__":
    main()
